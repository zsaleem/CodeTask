## Requirements
Below is the stack I used in this project. Please follow the getting started section in order to make this project run.

## Stack
 - HTML5
 - Sass
 - jQuery
 - requirejs
 - Git
 - Gitlab
 - [Bower](http://bower.io/).
 - Expressjs.
 - NPM
 - Vim
 - Facebook JavaScript SDK
 - Google Maps
 - Mac OS X.
 - Google Chrome Incognito
 - Grunt (as build tool) with following plugin!
   - [CSSmin](https://github.com/gruntjs/grunt-contrib-cssmin)
   - [Concat](https://github.com/gruntjs/grunt-contrib-concat)
   - [JSHint](https://github.com/gruntjs/grunt-contrib-jshint)
   - [Clean](https://github.com/gruntjs/grunt-contrib-clean)
   - [HTMLmin](https://github.com/gruntjs/grunt-contrib-htmlmin)
   - [ProcessHTML](https://github.com/dciccale/grunt-processhtml)
   - [Copy](https://github.com/gruntjs/grunt-contrib-copy)
   - [Nodemon](https://github.com/ChrisWren/grunt-nodemon)
   - [Concurrent](https://github.com/sindresorhus/grunt-concurrent)
   - [Watch](https://github.com/gruntjs/grunt-contrib-watch)
   - [Requirejs](https://github.com/gruntjs/grunt-contrib-requirejs)

## Getting started
In order to run this project in the browser successfully, please follow the steps below!

    1. Clone this repository. Make sure Git is installed on your systems.
    2. CD to the root folder of this project.
    3. Run `npm install` (if it complains then run `sudo npm install`) to install all Grunt plugins(dependencies). Please make sure that nodejs is installed on your machine before running npm install.
    4. Run `bower install` to install all project dependencies i.e. jquery into js/libs folder. Please make sure that bower is install on your machine before running bower install command.
    5. Now run `grunt` command on your terminal in `root` folder of this project.
    6. Now go to your browser and type `localhost:8000` to view this project in action.

## Description
Above steps, in getting started section, will install all dependencies required for this project to run and make the project ready for
production by minifying all resources. It will place the production ready project in `dist` folder in `root` level.

In this task, I followed MV* pattern. I created 2 folders inside `src/js/` i.e. `models` and `views`. Inside `models` I wrote `FacebookLoginModel` which is responsible initiate facebook JavaScript SDK and Login user. Furthermore, In model I am using jquery promises and I am calling one ajax request to login user and then get user's profile picture.

In `views` folder, I wrote `FacebookLoginView.js` module. This view gets data from the model, requires `MapView`(responsible for rendering map) and pass user's profile picture to this view (received from model).

I wrote all CSS(Sass) in `src/styles/sass` folder. I created 1 folder inside `src/styles/sass` folder i.e. `modules` contains `_map.scss` which styles map's container div.

I tested this task on Google Chrome Incognito and mac book air with a resolution of 1440x900.



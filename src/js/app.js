/*global define:false */
define(function(require) {

	'use strict';

    var FacebookLoginModel = require('./models/FacebookLoginModel'),
        FacebookLoginView = require('./views/FacebookLoginView');

    var App = (function() {

        var init = function() {
            var loginModel = new FacebookLoginModel(),
                loginView = new FacebookLoginView(loginModel);

            loginView.triggerLogin();
        };

        return {
            init: init
        };

    }());

	return App;
});

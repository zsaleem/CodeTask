/*global require:false*/
require.config({
    urlArgs: 'version=' + (new Date()).getTime(),
    baseUrl: 'js',
    shim: {
        'facebook': {
            exports: 'FB'
        }
    },
    paths: {
        'jquery': 'libs/jquery/dist/jquery',
        'async' : 'libs/requirejs-plugins/src/async',
        'facebook': 'facebook/fb'
    }
});


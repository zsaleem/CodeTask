define(function(require) {

    'use strict';

    var FB = require('facebook');
    var $ = require('jquery');

    var config = {
        appId   : '390870641248038',
        xfbml   : true,
        version : 'v2.8'
    };

    var FacebookLoginModel = function() {
        FB.init(config);
    };

    FacebookLoginModel.prototype = (function() {

        var userInfo = {};

        var isLoggedin = function() {
            FB.getLoginStatus(function(response) {
                console.log(response);
            });
        },

        login = function() {
            var deferred = new $.Deferred();

            return deferred.promise(FB.login(function(res) {
                if (res.authResponse) {
                    FB.api('/me?fields=location,picture', function(response) {
                        userInfo['picture'] = '';
                        userInfo['picture'] = response.picture.data.url;

                        return deferred.resolve(userInfo);
                    });
                } else {
                    return deferred.reject(res);
                }
            }));
        };

        return {
            isLoggedin: isLoggedin,
            login: login
        }

    })();

    return FacebookLoginModel;
});


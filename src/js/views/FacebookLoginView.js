define(function(require) {

    'use strict';

    var MapView = require('./MapView');

    var FacebookLoginView = function(model) {
        this._model = model;
    };

    FacebookLoginView.prototype = (function() {

        var triggerLogin = function() {
            this._model.login().done(function(res) {
                new MapView(res).getUserLocation();
            });
        };

        return {
            triggerLogin: triggerLogin
        }

    })();

    return FacebookLoginView;
});


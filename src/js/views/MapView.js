define(function(require) {

    'use strict';

    var gMap = require('async!https://maps.googleapis.com/maps/api/js?key=AIzaSyCunpYW3wAxR_V3kJJWOFkmpLoFPRgI2VM');

    var MapView = function(res) {
        this._res = res;
    };

    MapView.prototype = (function() {

        var render = function(position) {
            var self = this;
            var latitude = position.coords.latitude;
            var longitude = position.coords.longitude;

            var loc = {
                lat: latitude,
                lng: longitude
            };

            var map = new google.maps.Map(document.querySelector('.map'), {
                center: loc,
                zoom: 8
            });

            var marker = new google.maps.Marker({
                position: loc,
                icon: self._res.picture,
                map: map
            });
        },

        getUserLocation = function() {
            if(navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(render.bind(this));
            } else {
                alert('Geo Location is not supported');
            }
        };

        return {
            render: render,
            getUserLocation: getUserLocation
        }
    })();

    return MapView;
});

